﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{

    public GameObject player;

    private float offsetX;
    private float offsetY;

    private void Start()
    {
        offsetX = transform.position.x - player.transform.position.x;
        offsetY = transform.position.y - player.transform.position.y;
    }

    void Update()
    {
        transform.position = new Vector3(player.transform.position.x + offsetX,
            player.transform.position.y + offsetY,
            transform.position.z);
    }
}
