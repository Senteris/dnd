﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{   
    bool isAnimationEnd = false;
    bool isStart = false;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = true;
    }

    public void Game()
    {
        if (!isStart)
        {
            isStart = true;
            GetComponent<Animation>().Play();
            StartCoroutine(LoadScene());
        }
    }

    IEnumerator LoadScene()
    {
        yield return null;

        //Begin to load the Scene you specify
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync("main", LoadSceneMode.Single);
        //Don't let the Scene activate until you allow it to
        asyncOperation.allowSceneActivation = false;
        //When the load is still in progress, output the Text and progress bar
        while (!asyncOperation.isDone)
        {
            Debug.Log(asyncOperation.progress);
            if (asyncOperation.progress >= 0.9f && isAnimationEnd)
            {               
                asyncOperation.allowSceneActivation = true;
                yield return null;

            }
            
            yield return null;
        }
    }

    public void Exit()
    {
        if (!isStart)
        {
            isStart = true;
            GetComponent<Animation>().Play();
            Application.Quit();
        }
    }

    public void AnimationEnd()
    {
        isAnimationEnd = true;
    }
}
