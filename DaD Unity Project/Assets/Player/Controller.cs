﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Controller : MonoBehaviour
{
    public float maxSpeed = 10f;
    public float jumpForce = 700f;
    public VJHandler jsMovement;

    private bool facingRight = true;
    private float move;
    private new Rigidbody2D rigidbody2D;
    private Animator anim;
    private bool isGrounded;

    void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("Entered");
        isGrounded = true;
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        //Debug.Log("Exited");
        isGrounded = false;
    }

    // Use this for initialization
    void Start()
    {
        Debug.Log(SystemInfo.deviceType);
        rigidbody2D = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
    // Update is called once per frame
    void FixedUpdate()
    {

        move = Input.GetAxis("Horizontal");

    }

    [System.Obsolete]
    void Update()
    {
        Vector3 direction = jsMovement.InputDirection;
        if (Input.GetAxis("Jump") > 0 && isGrounded)
        {
            rigidbody2D.AddForce(new Vector2(0f, jumpForce));
        }
        rigidbody2D.velocity = new Vector2(move * maxSpeed, rigidbody2D.velocity.y);

        if (move > 0 && !facingRight)
        {
            facingRight = !facingRight;
            transform.localScale = new Vector3(1, transform.localScale.y, transform.localScale.z);
        }
        else if (move < 0 && facingRight)
        {
            facingRight = !facingRight;
            transform.localScale = new Vector3(-1, transform.localScale.y, transform.localScale.z);
        }


        if (Input.GetAxis("Cancel") > 0)
        {
            SceneManager.LoadScene("menu", LoadSceneMode.Single);
        }

        if (Input.GetKey(KeyCode.R))
        {
            Application.LoadLevel(Application.loadedLevel);
        }
        if (move != 0)
        {
            anim.Play("psina_move");
        }
        else
        {
            anim.Play("psina_stay");
        }
    }
}