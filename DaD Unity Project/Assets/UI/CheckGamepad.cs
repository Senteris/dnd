﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckGamepad : MonoBehaviour
{
    public GameObject gamepadConn;
    public GameObject gamepadDisConn;

    string isGamepad = "";
    string previousIsGamepad = "";
    // Start is called before the first frame update
    void Start()
    {
        //Get Joystick Names
        string[] temp = Input.GetJoystickNames();

        //Check whether array contains anything
        if (temp.Length > 0)
        {
            //Iterate over every element
            for (int i = 0; i < temp.Length; ++i)
            {
                //Check if the string is empty or not
                if (!string.IsNullOrEmpty(temp[i]))
                {
                    //Not empty, controller temp[i] is connected
                    //Debug.Log("Controller " + i + " is connected using: " + temp[i]);
                    isGamepad = temp[i];
                }
                else
                {
                    //If it is empty, controller i is disconnected
                    //where i indicates the controller number
                    //Debug.Log("Controller: " + i + " is disconnected.");

                }
            }
        }

        previousIsGamepad = isGamepad;
    }

    // Update is called once per frame
    void Update()
    {
        isGamepad = "";
        //Get Joystick Names
        string[] temp = Input.GetJoystickNames();      

        //Check whether array contains anything
        if (temp.Length > 0)
        {
            //Iterate over every element
            for (int i = 0; i < temp.Length; ++i)
            {
                //Check if the string is empty or not
                if (!string.IsNullOrEmpty(temp[i]))
                {
                    //Not empty, controller temp[i] is connected
                    //Debug.Log("Controller " + i + " is connected using: " + temp[i]);
                    isGamepad = temp[i];
                }
                else
                {
                    //If it is empty, controller i is disconnected
                    //where i indicates the controller number
                    //Debug.Log("Controller: " + i + " is disconnected.");

                }
            }
        }

        if(previousIsGamepad != isGamepad)
        {
            if (isGamepad!= "")
            {
                Debug.Log("Controller is connected using: " + isGamepad);
                gamepadConn.GetComponent<Animation>().Play();
            }
            else
            {
                Debug.LogWarning("Controller is disconnected.");
                gamepadDisConn.GetComponent<Animation>().Play();
            }
        }
        previousIsGamepad = isGamepad;
    }
}
